const STORAGE_KEY = 'todo_list';

const TodoStorage = {
    fetch() {
        const todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
        todos.forEach((todo, index) => (todo.id = index));
        TodoStorage.uid = todos.length;

        return todos;
    },

    save(todos) {
        return localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
    }
};

export default TodoStorage;
